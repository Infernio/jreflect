package jreflect.internal;

import jreflect.Type;
import jreflect.structure.IAbstractExpression;
import jreflect.structure.IAbstractStatement;
import jreflect.structure.IAbstractValue;
import jreflect.structure.INode;
import jreflect.structure.INumericVariable;

public class NumericVariableImpl<T extends Number> extends AbstractVariableImpl<T> implements INumericVariable<T>
{
    public NumericVariableImpl(final INode parent, final Type<T> type, final String name)
    {
        super(parent, type, name);
    }

    @Override
    public IAbstractExpression<T> add(final IAbstractValue<T> value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<T> subtract(final IAbstractValue<T> value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<T> multiply(final IAbstractValue<T> value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<T> divide(final IAbstractValue<T> value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<T> modulo(final IAbstractValue<T> value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<Boolean> compare(final IAbstractExpression<T> to, final Comparison operator)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Void> increase(final T by)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Void> increase(final IAbstractExpression<T> by)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Void> decrease(final T by)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Void> decrease(final IAbstractExpression<T> by)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Integer> increment()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Integer> decrement()
    {
        // TODO Auto-generated method stub
        return null;
    }

}
