package jreflect;

import java.util.Set;

import jreflect.structure.constant.AccessLevel;
import jreflect.structure.constant.Modifier;

public class JRSettings
{
    public static final Setting<String> JAVA_CODE_INDENTATION = new Setting<>("    ");

    public static final class ClassLevel
    {
        public static final Setting<AccessLevel> DEFAULT_ACCESS_LEVEL = new Setting<>(AccessLevel.PUBLIC);
        public static final Setting<Set<Modifier>> DEFAULT_MODIFIERS = new Setting<>(Set.of());
    }

    public static final class FieldLevel
    {
        public static final Setting<AccessLevel> DEFAULT_ACCESS_LEVEL = new Setting<>(AccessLevel.PRIVATE);
        public static final Setting<Set<Modifier>> DEFAULT_MODIFIERS = new Setting<>(Set.of());
    }

    public static final class MethodLevel
    {
        public static final Setting<AccessLevel> DEFAULT_ACCESS_LEVEL = new Setting<>(AccessLevel.PUBLIC);
        public static final Setting<Set<Modifier>> DEFAULT_MODIFIERS = new Setting<>(Set.of());
    }

    public static final class VariableLevel
    {
        public static final Setting<Set<Modifier>> DEFAULT_MODIFIERS = new Setting<>(Set.of(Modifier.FINAL));
    }

    public static final class Setting<T>
    {
        private T value;

        public Setting(final T val)
        {
            this.value = val;
        }

        public T get()
        {
            return this.value;
        }

        public void set(final T to)
        {
            this.value = to;
        }
    }
}
