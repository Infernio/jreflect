package jreflect.structure.constant;

public enum Modifier
{
    ABSTRACT,
    DEFAULT,
    FINAL,
    NATIVE,
    STATIC,
    STRICTFP,
    SYNCHRONIZED,
    TRANSIENT,
    VOLATILE;
}
