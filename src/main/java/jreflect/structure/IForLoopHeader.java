package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IForLoopHeader extends IAbstractLoopHeader
{
    IAbstractStatement<?>[] getInitializers();

    void setInitializers(IAbstractStatement<?>... initializers);

    IAbstractStatement<?>[] getActions();

    void setActions(IAbstractStatement<?>... actions);

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
