package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.INumericMethods;

public interface INumericExpression<T extends Number> extends IAbstractExpression<T>, INumericMethods<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
