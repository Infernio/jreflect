package jreflect.structure;

public interface IAbstractLoopHeader extends INode
{
    IAbstractExpression<Boolean> getCondition();

    void setCondition(IAbstractExpression<Boolean> condition);
}
