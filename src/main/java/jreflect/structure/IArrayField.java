package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.IArrayMethods;

public interface IArrayField<T> extends IAbstractField<T>, IArrayMethods<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
