package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IWhileLoop extends IAbstractLoop
{
    @Override
    IWhileLoopHeader getHeader();

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
