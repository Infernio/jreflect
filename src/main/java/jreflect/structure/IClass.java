package jreflect.structure;

import java.util.List;

import jreflect.Type;
import jreflect.backend.IVisitor;
import jreflect.structure.internal.IAccessLeveled;
import jreflect.structure.internal.IModifiers;
import jreflect.structure.internal.INamed;

public interface IClass extends IAccessLeveled, IModifiers, INamed, INode
{
    List<IConstructor<?>> getConstructors();

    List<IAbstractField<?>> getFields();

    List<IMethod<?>> getMethods();

    IPackage getPackage();

    <T> IAbstractField<T> newField(Type<T> type, String name);

    <T> IMethod<T> newMethod(Type<T> returnType, String name);

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
