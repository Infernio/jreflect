package jreflect.structure.internal;

import jreflect.structure.IBlock;

public interface IBodied
{
    IBlock getBody();
}
