package jreflect.internal;

import java.util.HashSet;
import java.util.Set;

import jreflect.JRSettings;
import jreflect.Type;
import jreflect.structure.IAbstractStatement;
import jreflect.structure.IAbstractValue;
import jreflect.structure.IAbstractVariable;
import jreflect.structure.IModifier;
import jreflect.structure.INode;
import jreflect.structure.constant.Modifier;

public abstract class AbstractVariableImpl<T> extends NodeImpl implements IAbstractVariable<T>
{
    // TODO For all impls that use modifiers: Create a new set that throws
    // errors when disallowed modifiers are inserted (e.g. synchronized variable)
    private final Type<T> type;
    private String name;
    private Set<IModifier> modifiers;

    public AbstractVariableImpl(final INode parent, final Type<T> type, final String name)
    {
        super(parent);
        this.type = type;
        this.name = name;

        this.modifiers = new HashSet<>();
        ModifierImpl.addAll(this, JRSettings.VariableLevel.DEFAULT_MODIFIERS.get(), this.modifiers);
    }

    @Override
    public IAbstractStatement<?> invokeMethod(final String string, final IAbstractValue<?>... values)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<Void> assign(final T value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Type<T> getType()
    {
        return this.type;
    }

    @Override
    public void addModifiers(final Modifier... mods)
    {
        ModifierImpl.addAll(this, Set.of(mods), this.modifiers);
    }

    @Override
    public void removeModifiers(final Modifier... mods)
    {
        ModifierImpl.removeAll(Set.of(mods), this.modifiers);
    }

    @Override
    public Set<IModifier> getModifiers()
    {
        return this.modifiers;
    }

    @Override
    public void setModifiers(final Set<IModifier> to)
    {
        this.modifiers = to;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(final String to)
    {
        this.name = to;
    }
}
