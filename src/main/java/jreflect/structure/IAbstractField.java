package jreflect.structure;

import jreflect.structure.internal.IAccessLeveled;
import jreflect.structure.internal.IAssignable;
import jreflect.structure.internal.IModifiers;
import jreflect.structure.internal.INamed;

public interface IAbstractField<T> extends IAbstractValue<T>, IAccessLeveled, IAssignable<T>, IModifiers, INamed {}
