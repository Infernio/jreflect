package jreflect.structure.internal;

public interface INamed
{
    String getName();

    void setName(String to);
}
