package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IForLoop extends IAbstractLoop
{
    @Override
    IForLoopHeader getHeader();

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
