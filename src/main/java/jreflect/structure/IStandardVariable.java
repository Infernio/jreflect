package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IStandardVariable<T> extends IAbstractVariable<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
