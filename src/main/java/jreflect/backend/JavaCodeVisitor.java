package jreflect.backend;

import jreflect.JRSettings;
import jreflect.structure.*;
import lombok.val;

public class JavaCodeVisitor implements IVisitor
{
    private final StringBuilder result;
    private StringBuilder builder;
    private int indentationLevel;

    public JavaCodeVisitor()
    {
        this.result = new StringBuilder();
        this.builder = new StringBuilder();
    }

    public String finish()
    {
        String ret;
        if(this.result.toString().isEmpty())
        {
            // This can happen if toString() is called on things
            // that don't normally need a finishLine() call
            ret = this.builder.toString();
        }
        else
        {
            ret = this.result.toString();
        }

        // Trim off whitespace to make it nicer in lists, etc.
        return ret.strip();
    }

    private void append(final String str)
    {
        this.builder.append(str);
    }

    private void append(final char c)
    {
        this.builder.append(c);
    }

    private void finishLine()
    {
        val temp = this.builder.toString();
        this.builder = new StringBuilder();

        for(var i = 0; i < this.indentationLevel; ++i)
        {
            this.result.append(JRSettings.JAVA_CODE_INDENTATION.get());
        }
        this.result.append(temp);
        this.result.append('\n');
    }

    @Override
    public void visit(final IAbstractField<?> field)
    {
        field.getAccess().accept(this);
        field.getModifiers().forEach(m -> m.accept(this));

        this.append(field.getType().getClassName());
        this.append(' ');

        this.append(field.getName());
        this.append(';');
        this.finishLine();
    }

    @Override
    public void visit(final IAbstractVariable<?> variable)
    {
        variable.getModifiers().forEach(m -> m.accept(this));

        this.append(variable.getType().getClassName());
        this.append(' ');

        this.append(variable.getName());
        // can't finish line here - local vars are used for
        // both real local vars and parameters.
    }

    @Override
    public void visit(final IArrayVariable<?> variable)
    {
        variable.getModifiers().forEach(m -> m.accept(this));

        this.append(variable.getType().getClassName());
        this.append("[] ");

        this.append(variable.getName());
        // can't finish line here - local vars are used for
        // both real local vars and parameters.
    }

    @Override
    public void visit(final IAccessLevel level)
    {
        val lvl = level.getLevel();
        if(!lvl.identifier.isBlank())
        {
            this.append(lvl.identifier);
            this.append(' ');
        }
    }

    @Override
    public void visit(final IBlock block)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(final IClass clazz)
    {
        clazz.getPackage().accept(this);

        clazz.getAccess().accept(this);
        clazz.getModifiers().forEach(m -> m.accept(this));

        this.append("class ");
        this.append(clazz.getName());
        this.finishLine();

        this.append('{');
        this.finishLine();
        ++this.indentationLevel;

        clazz.getFields().forEach(f -> f.accept(this));
        if(!clazz.getFields().isEmpty())
        {
            this.finishLine();
        }
        clazz.getMethods().forEach(m -> m.accept(this));

        --this.indentationLevel;
        if(clazz.getMethods().isEmpty() && clazz.getFields().isEmpty())
        {
            // Format this nicely
            this.finishLine();
        }
        this.append('}');
        this.finishLine();
    }

    @Override
    public void visit(final IMethod<?> method)
    {
        method.getAccess().accept(this);
        method.getModifiers().forEach(m -> m.accept(this));

        this.append(method.getReturnType().getClassName());
        this.append(' ');
        this.append(method.getName());

        this.append('(');
        val params = method.getParameters();
        for(var i = 0; i < params.size(); ++i)
        {
            params.get(i).accept(this);
            if(i + 1 < params.size())
            {
                this.append(", ");
            }
        }
        this.append(')');

        this.finishLine();
        this.append("{");
        this.finishLine();

        ++this.indentationLevel;
        method.getBody().accept(this);
        --this.indentationLevel;

        this.finishLine();
        this.append('}');
        this.finishLine();
    }

    @Override
    public void visit(final IModifier mod)
    {
        this.append(mod.asJavaKeyword());
        this.append(' ');
    }

    @Override
    public void visit(final IPackage pkg)
    {
        // 'default' package simply means no package declaration
        if(!pkg.isDefault())
        {
            this.append("package ");
            this.append(pkg.getName());
            this.append(';');
            this.finishLine();
            this.finishLine();
        }
    }
}
