package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.constant.AccessLevel;

public interface IAccessLevel extends INode
{
    AccessLevel getLevel();

    void setLevel(AccessLevel to);

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
