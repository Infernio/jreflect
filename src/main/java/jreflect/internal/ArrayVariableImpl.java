package jreflect.internal;

import jreflect.Type.ArrayType;
import jreflect.structure.IAbstractExpression;
import jreflect.structure.IAbstractValue;
import jreflect.structure.IArrayVariable;
import jreflect.structure.INode;

public class ArrayVariableImpl<T> extends AbstractVariableImpl<T> implements IArrayVariable<T>
{
    public ArrayVariableImpl(final INode parent, final ArrayType<T> type, final String name)
    {
        super(parent, type, name);
    }

    @Override
    public IAbstractExpression<Integer> arrayLength()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<T> arrayValue(final IAbstractValue<Integer> var)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
