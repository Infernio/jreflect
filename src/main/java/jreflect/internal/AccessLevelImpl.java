package jreflect.internal;

import jreflect.backend.JavaCodeVisitor;
import jreflect.structure.IAccessLevel;
import jreflect.structure.INode;
import jreflect.structure.constant.AccessLevel;
import lombok.val;

public final class AccessLevelImpl extends NodeImpl implements IAccessLevel
{
    private AccessLevel level;

    public AccessLevelImpl(final INode parent)
    {
        super(parent);
        this.level = AccessLevel.PACKAGE_PROTECTED;
    }

    @Override
    public AccessLevel getLevel()
    {
        return this.level;
    }

    @Override
    public void setLevel(final AccessLevel to)
    {
        this.level = to;
    }

    @Override
    public String toString()
    {
        val visitor = new JavaCodeVisitor();
        visitor.visit(this);
        return visitor.finish();
    }
}
