package jreflect.structure.internal;

import jreflect.structure.IAbstractExpression;
import jreflect.structure.IAbstractValue;

public interface IArrayMethods<T>
{
    IAbstractExpression<Integer> arrayLength();

    IAbstractExpression<T> arrayValue(IAbstractValue<Integer> var);
}
