package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.IArrayMethods;

public interface IArrayVariable<T> extends IAbstractVariable<T>, IArrayMethods<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
