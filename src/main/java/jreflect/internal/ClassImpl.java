package jreflect.internal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jreflect.JRSettings;
import jreflect.Type;
import jreflect.structure.IAbstractField;
import jreflect.structure.IAccessLevel;
import jreflect.structure.IClass;
import jreflect.structure.IConstructor;
import jreflect.structure.IMethod;
import jreflect.structure.IModifier;
import jreflect.structure.IPackage;
import jreflect.structure.constant.Modifier;
import lombok.val;

public final class ClassImpl extends NodeImpl implements IClass
{
    private final IPackage pkg;
    private String name;
    private final IAccessLevel access;
    private Set<IModifier> modifiers;

    private final List<IAbstractField<?>> fields;
    private final List<IConstructor<?>> constructors;
    private final List<IMethod<?>> methods;

    public ClassImpl(final String pkg, final String name)
    {
        super(null);
        this.pkg = new PackageImpl(this, pkg);
        this.name = name;

        this.access = new AccessLevelImpl(this);
        this.access.setLevel(JRSettings.ClassLevel.DEFAULT_ACCESS_LEVEL.get());

        this.modifiers = new HashSet<>();
        ModifierImpl.addAll(this, JRSettings.ClassLevel.DEFAULT_MODIFIERS.get(), this.modifiers);

        this.fields = new ArrayList<>();
        this.constructors = new ArrayList<>();
        this.methods = new ArrayList<>();
    }

    @Override
    public IAccessLevel getAccess()
    {
        return this.access;
    }

    @Override
    public void addModifiers(final Modifier... mods)
    {
        ModifierImpl.addAll(this, Set.of(mods), this.modifiers);
    }

    @Override
    public void removeModifiers(final Modifier... mods)
    {
        ModifierImpl.removeAll(Set.of(mods), this.modifiers);
    }

    @Override
    public Set<IModifier> getModifiers()
    {
        return this.modifiers;
    }

    @Override
    public void setModifiers(final Set<IModifier> to)
    {
        this.modifiers = to;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(final String to)
    {
        this.name = to;
    }

    @Override
    public List<IConstructor<?>> getConstructors()
    {
        return this.constructors;
    }

    @Override
    public List<IAbstractField<?>> getFields()
    {
        return this.fields;
    }

    @Override
    public List<IMethod<?>> getMethods()
    {
        return this.methods;
    }

    @Override
    public IPackage getPackage()
    {
        return this.pkg;
    }

    @Override
    public <T> IAbstractField<T> newField(final Type<T> type, final String name)
    {
        val ret = new FieldImpl<>(this, type, name);
        this.fields.add(ret);
        return ret;
    }

    @Override
    public <T> IMethod<T> newMethod(final Type<T> returnType, final String name)
    {
        val ret = new MethodImpl<>(this, returnType, name);
        this.methods.add(ret);
        return ret;
    }
}
