package jreflect.structure.internal;

import java.util.Set;

import jreflect.structure.IModifier;
import jreflect.structure.constant.Modifier;

public interface IModifiers
{
    void addModifiers(Modifier... modifiers);

    void removeModifiers(Modifier... modifiers);

    Set<IModifier> getModifiers();

    void setModifiers(Set<IModifier> to);
}
