package jreflect.internal;

import jreflect.backend.JavaCodeVisitor;
import jreflect.structure.INode;
import jreflect.structure.IPackage;
import lombok.val;

public final class PackageImpl extends NodeImpl implements IPackage
{
    private String name;

    public PackageImpl(final INode parent, final String name)
    {
        super(parent);
        this.name = name;
    }

    @Override
    public boolean isDefault()
    {
        return this.name.isBlank();
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(final String to)
    {
        this.name = to;
    }

    @Override
    public String toString()
    {
        val visitor = new JavaCodeVisitor();
        visitor.visit(this);
        return visitor.finish();
    }
}
