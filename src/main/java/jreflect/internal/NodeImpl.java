package jreflect.internal;

import jreflect.backend.JavaCodeVisitor;
import jreflect.structure.INode;
import lombok.val;

public abstract class NodeImpl implements INode
{
    private final INode parent;

    public NodeImpl(final INode parent)
    {
        this.parent = parent;
    }

    @Override
    public INode getParent()
    {
        return this.parent;
    }

    @Override
    public String toString()
    {
        val visitor = new JavaCodeVisitor();
        this.accept(visitor);
        return visitor.finish();
    }
}
