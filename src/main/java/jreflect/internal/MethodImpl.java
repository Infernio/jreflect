package jreflect.internal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jreflect.JRSettings;
import jreflect.Type;
import jreflect.Type.ArrayType;
import jreflect.Type.NumericType;
import jreflect.structure.*;
import jreflect.structure.constant.Modifier;
import lombok.val;

public class MethodImpl<R> extends NodeImpl implements IMethod<R>
{
    private final Type<R> returnType;
    private String name;
    private final IAccessLevel access;
    private Set<IModifier> modifiers;
    private final List<IAbstractVariable<?>> parameters;
    private final IBlock body;

    public MethodImpl(final INode parent, final Type<R> returnType, final String name)
    {
        super(parent);
        this.returnType = returnType;
        this.name = name;

        this.access = new AccessLevelImpl(this);
        this.access.setLevel(JRSettings.MethodLevel.DEFAULT_ACCESS_LEVEL.get());

        this.modifiers = new HashSet<>();
        ModifierImpl.addAll(this, JRSettings.MethodLevel.DEFAULT_MODIFIERS.get(), this.modifiers);

        this.parameters = new ArrayList<>();
        this.body = new BlockImpl(this);
    }

    @Override
    public IAccessLevel getAccess()
    {
        return this.access;
    }

    @Override
    public IBlock getBody()
    {
        return this.body;
    }

    @Override
    public void addModifiers(final Modifier... mods)
    {
        ModifierImpl.addAll(this, Set.of(mods), this.modifiers);
    }

    @Override
    public void removeModifiers(final Modifier... mods)
    {
        ModifierImpl.removeAll(Set.of(mods), this.modifiers);
    }

    @Override
    public Set<IModifier> getModifiers()
    {
        return this.modifiers;
    }

    @Override
    public void setModifiers(final Set<IModifier> to)
    {
        this.modifiers = to;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(final String to)
    {
        this.name = to;
    }

    @Override
    public List<IAbstractVariable<?>> getParameters()
    {
        return this.parameters;
    }

    @Override
    public Type<R> getReturnType()
    {
        return this.returnType;
    }

    @Override
    public <T> IArrayVariable<T> newParameter(final ArrayType<T> type, final String name)
    {
        val ret = new ArrayVariableImpl<>(this, type, name);
        this.parameters.add(ret);
        return ret;
    }

    @Override
    public <T extends Number> INumericVariable<T> newParameter(final NumericType<T> type, final String name)
    {
        val ret = new NumericVariableImpl<>(this, type, name);
        this.parameters.add(ret);
        return ret;
    }

    @Override
    public <T> IStandardVariable<T> newParameter(final Type<T> type, final String name)
    {
        val ret = new StandardVariableImpl<>(this, type, name);
        this.parameters.add(ret);
        return ret;
    }
}
