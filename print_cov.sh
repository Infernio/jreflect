#!/bin/bash

# Find the coverage in the HTML report
# Gitlab will worry about extracting the percentage
cat 'build/reports/jacoco/test/html/index.html' | grep -o 'Total[^%]*%'
