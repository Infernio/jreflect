package jreflect.test;

import java.io.PrintStream;

import org.junit.Test;

import jreflect.ClassFactory;
import jreflect.Type;
import jreflect.backend.JVMBytecodeVisitor;
import jreflect.exception.MalformedBytecodeException;
import jreflect.structure.constant.Modifier;
import lombok.val;

public class ComplexUsage
{
    @Test
    public void testClassGen()
    {
        // Create 'Generated.class' in the package 'test'
        val clazz = ClassFactory.newClass("test", "Generated");

        // Make a 'public static void main(String[] args)'
        val main = clazz.newMethod(Type.VOID, "main");
        main.addModifiers(Modifier.STATIC);
        val args = main.newParameter(Type.array(Type.of(String.class)), "args");

        // Begin making a for loop
        val metBody = main.getBody();
        val i = metBody.newVariable(Type.INT, "i");
        val loop = metBody.newForLoop();

        // Create the for loop header: 'int i = 0; i < args.length; ++i'
        val header = loop.getHeader();
        header.setInitializers(i.assign(Integer.valueOf(0)));
        header.setCondition(i.lessThan(args.arrayLength()));
        header.setActions(i.increment());

        // Create the for loop body: 'System.out.println(args[i])'
        val loopBody = loop.getBody();
        val out = loopBody.staticFieldValue(Type.of(System.class), "out", Type.of(PrintStream.class));
        out.invokeMethod("println", args.arrayValue(i));

        Class<?> output = Void.class;
        val visitor = new JVMBytecodeVisitor();
        visitor.visit(clazz);
        try
        {
            output = visitor.toClass();
        }
        catch(MalformedBytecodeException e)
        {
            e.printStackTrace();
        }
        System.out.println(output.getName());
    }
}
