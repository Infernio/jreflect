# jreflect
A small library to simplify the usage of [ASM](https://asm.ow2.io/).

[![pipeline status](https://gitlab.com/Infernio/jreflect/badges/master/pipeline.svg)](https://gitlab.com/Infernio/jreflect/pipelines)
[![coverage report](https://gitlab.com/Infernio/jreflect/badges/master/coverage.svg)](https://infernio.gitlab.io/jreflect/)

## Building
- On Linux / Mac: `./gradlew build`
- On Windows: `gradlew.bat build`
