package jreflect.structure.internal;

import jreflect.structure.IAccessLevel;
import jreflect.structure.constant.AccessLevel;

public interface IAccessLeveled
{
    IAccessLevel getAccess();

    default AccessLevel getAccessLevel()
    {
        return this.getAccess().getLevel();
    }

    default void setAccessLevel(final AccessLevel to)
    {
        this.getAccess().setLevel(to);
    }
}
