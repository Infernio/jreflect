package jreflect.structure;

import jreflect.structure.internal.IBodied;

public interface IAbstractLoop extends IAbstractStatement<Void>, IBodied
{
    IAbstractLoopHeader getHeader();
}
