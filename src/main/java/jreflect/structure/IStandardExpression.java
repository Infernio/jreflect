package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IStandardExpression<T> extends IAbstractExpression<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
