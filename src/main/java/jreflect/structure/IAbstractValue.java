package jreflect.structure;

import jreflect.structure.internal.ITyped;

public interface IAbstractValue<T> extends ITyped<T>, INode
{
    IAbstractStatement<?> invokeMethod(String string, IAbstractValue<?>... values);
}
