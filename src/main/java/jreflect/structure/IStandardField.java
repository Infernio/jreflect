package jreflect.structure;

import jreflect.backend.IVisitor;

public interface IStandardField<T> extends IAbstractField<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
