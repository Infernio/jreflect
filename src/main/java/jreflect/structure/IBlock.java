package jreflect.structure;

import jreflect.Type;
import jreflect.Type.ArrayType;
import jreflect.Type.NumericType;
import jreflect.backend.IVisitor;

public interface IBlock extends INode
{
    IWhileLoop newWhileLoop();

    IForLoop newForLoop();

    <T> IArrayVariable<T> newVariable(Type.ArrayType<T> type, String name);

    <T extends Number> INumericVariable<T> newVariable(Type.NumericType<T> type, String name);

    <T> IStandardVariable<T> newVariable(Type<T> type, String name);

    IAbstractExpression<?> fieldValue(Type<?> owningType, String fieldName);

    <T> IStandardExpression<T> fieldValue(Type<?> owningType, String fieldName, Type<T> fieldType);

    <T> IArrayExpression<T> fieldValue(Type<?> owningType, String fieldName, ArrayType<T> fieldType);

    <T extends Number> INumericExpression<T> fieldValue(Type<?> owningType, String fieldName, NumericType<T> fieldType);

    IAbstractExpression<?> staticFieldValue(Type<?> owningType, String fieldName);

    <T> IStandardExpression<T> staticFieldValue(Type<?> owningType, String fieldName, Type<T> fieldType);

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
