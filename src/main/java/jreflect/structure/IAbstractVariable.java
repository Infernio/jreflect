package jreflect.structure;

import jreflect.structure.internal.IAssignable;
import jreflect.structure.internal.IModifiers;
import jreflect.structure.internal.INamed;

public interface IAbstractVariable<T> extends IAbstractValue<T>, IAssignable<T>, IModifiers, INamed {}
