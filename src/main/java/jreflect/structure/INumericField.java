package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.INumericMethods;

public interface INumericField<T extends Number> extends IAbstractField<T>, INumericMethods<T>
{
    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
