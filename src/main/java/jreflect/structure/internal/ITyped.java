package jreflect.structure.internal;

import jreflect.Type;

public interface ITyped<T>
{
    Type<T> getType();
}
