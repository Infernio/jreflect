package jreflect.backend;

import jreflect.structure.*;

@SuppressWarnings("unused")
public interface IVisitor
{
    // @formatter:off
    default void visit(final IAbstractExpression<?> expr)
    {
        this.visit((IAbstractValue<?>)expr);
    }

    default void visit(final IAbstractField<?> expr)
    {
        this.visit((IAbstractValue<?>)expr);
    }

    default void visit(final IAbstractLoop loop)
    {
        this.visit((IAbstractStatement<?>)loop);
    }

    default void visit(final IAbstractLoopHeader header)
    {
        this.visit((INode)header);
    }

    default void visit(final IAbstractStatement<?> stmt)
    {
        this.visit((IAbstractValue<?>)stmt);
    }

    default void visit(final IAbstractVariable<?> expr)
    {
        this.visit((IAbstractValue<?>)expr);
    }

    default void visit(final IAccessLevel level)
    {
        this.visit((INode)level);
    }

    default void visit(final IArrayExpression<?> expr)
    {
        this.visit((IAbstractExpression<?>)expr);
    }

    default void visit(final IArrayField<?> field)
    {
        this.visit((IAbstractField<?>)field);
    }

    default void visit(final IArrayVariable<?> variable)
    {
        this.visit((IAbstractVariable<?>)variable);
    }

    default void visit(final IBlock block)
    {
        this.visit((INode)block);
    }

    default void visit(final IClass clazz)
    {
        this.visit((INode)clazz);
    }

    default void visit(final IConstructor<?> method)
    {
        this.visit((IMethod<?>)method);
    }

    default void visit(final IForLoop loop)
    {
        this.visit((IAbstractLoop)loop);
    }

    default void visit(final IForLoopHeader header)
    {
        this.visit((IAbstractLoopHeader)header);
    }

    default void visit(final IMethod<?> method)
    {
        this.visit((INode)method);
    }

    default void visit(final IModifier mod)
    {
        this.visit((INode)mod);
    }

    default void visit(final INode node)
    {
        // Nothing to fall back to
    }

    default void visit(final INumericExpression<?> expr)
    {
        this.visit((IAbstractExpression<?>)expr);
    }

    default void visit(final INumericVariable<?> variable)
    {
        this.visit((IAbstractVariable<?>)variable);
    }

    default void visit(final IPackage pkg)
    {
        this.visit((INode)pkg);
    }

    default void visit(final IStandardExpression<?> expr)
    {
        this.visit((IAbstractExpression<?>)expr);
    }

    default void visit(final IStandardField<?> field)
    {
        this.visit((IAbstractField<?>)field);
    }

    default void visit(final IStandardVariable<?> variable)
    {
        this.visit((IAbstractVariable<?>)variable);
    }

    default void visit(final IWhileLoop loop)
    {
        this.visit((IAbstractLoop)loop);
    }

    default void visit(final IWhileLoopHeader header)
    {
        this.visit((IAbstractLoopHeader)header);
    }
    // @formatter:on
}
