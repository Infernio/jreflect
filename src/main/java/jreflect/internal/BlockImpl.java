package jreflect.internal;

import jreflect.Type;
import jreflect.Type.ArrayType;
import jreflect.Type.NumericType;
import jreflect.structure.*;

public class BlockImpl extends NodeImpl implements IBlock
{
    public BlockImpl(final INode parent)
    {
        super(parent);
    }

    @Override
    public IWhileLoop newWhileLoop()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IForLoop newForLoop()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> IArrayVariable<T> newVariable(final ArrayType<T> type, final String name)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Number> INumericVariable<T> newVariable(final NumericType<T> type, final String name)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> IStandardVariable<T> newVariable(final Type<T> type, final String name)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<?> fieldValue(final Type<?> owningType, final String fieldName)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> IStandardExpression<T> fieldValue(final Type<?> owningType, final String fieldName, final Type<T> fieldType)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> IArrayExpression<T> fieldValue(final Type<?> owningType, final String fieldName, final ArrayType<T> fieldType)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends Number> INumericExpression<T> fieldValue(final Type<?> owningType, final String fieldName, final NumericType<T> fieldType)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractExpression<?> staticFieldValue(final Type<?> owningType, final String fieldName)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> IStandardExpression<T> staticFieldValue(final Type<?> owningType, final String fieldName, final Type<T> fieldType)
    {
        // TODO Auto-generated method stub
        return null;
    }
}
