package jreflect.structure.constant;

public enum AccessLevel
{
    PACKAGE_PROTECTED(""),
    PRIVATE("private"),
    PROTECTED("protected"),
    PUBLIC("public");

    public final String identifier;

    private AccessLevel(final String id)
    {
        this.identifier = id;
    }
}