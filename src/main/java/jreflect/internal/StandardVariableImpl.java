package jreflect.internal;

import jreflect.Type;
import jreflect.structure.INode;
import jreflect.structure.IStandardVariable;

public class StandardVariableImpl<T> extends AbstractVariableImpl<T> implements IStandardVariable<T>
{
    public StandardVariableImpl(final INode parent, final Type<T> type, final String name)
    {
        super(parent, type, name);
    }
}
