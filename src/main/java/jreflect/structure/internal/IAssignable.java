package jreflect.structure.internal;

import jreflect.structure.IAbstractStatement;

public interface IAssignable<T>
{
    IAbstractStatement<Void> assign(T value);
}
