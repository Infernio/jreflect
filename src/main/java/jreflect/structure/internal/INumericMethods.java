package jreflect.structure.internal;

import jreflect.structure.IAbstractExpression;
import jreflect.structure.IAbstractValue;

public interface INumericMethods<T extends Number>
{
    IAbstractExpression<T> add(IAbstractValue<T> value);

    IAbstractExpression<T> subtract(IAbstractValue<T> value);

    IAbstractExpression<T> multiply(IAbstractValue<T> value);

    IAbstractExpression<T> divide(IAbstractValue<T> value);

    IAbstractExpression<T> modulo(IAbstractValue<T> value);

    IAbstractExpression<Boolean> compare(IAbstractExpression<T> to, Comparison operator);

    default IAbstractExpression<Boolean> equalTo(final IAbstractExpression<T> value)
    {
        return compare(value, Comparison.EQUAL_TO);
    }

    default IAbstractExpression<Boolean> greaterThan(final IAbstractExpression<T> value)
    {
        return compare(value, Comparison.GREATER_THAN);
    }

    default IAbstractExpression<Boolean> greaterThanOrEqual(final IAbstractExpression<T> value)
    {
        return compare(value, Comparison.GREATER_THAN_OR_EQUAL);
    }

    default IAbstractExpression<Boolean> lessThan(final IAbstractExpression<T> value)
    {
        return compare(value, Comparison.LESS_THAN);
    }

    default IAbstractExpression<Boolean> lessThanOrEqual(final IAbstractExpression<T> value)
    {
        return compare(value, Comparison.LESS_THAN_OR_EQUAL);
    }

    enum Comparison
    {
        EQUAL_TO,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL,
        LESS_THAN,
        LESS_THAN_OR_EQUAL;
    }
}
