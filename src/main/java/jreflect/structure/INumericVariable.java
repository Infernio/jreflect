package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.INumericMethods;

public interface INumericVariable<T extends Number> extends IAbstractVariable<T>, INumericMethods<T>
{
    IAbstractStatement<Void> increase(T by);

    IAbstractStatement<Void> increase(IAbstractExpression<T> by);

    IAbstractStatement<Void> decrease(T by);

    IAbstractStatement<Void> decrease(IAbstractExpression<T> by);

    IAbstractStatement<Integer> increment();

    IAbstractStatement<Integer> decrement();

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
