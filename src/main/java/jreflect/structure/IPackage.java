package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.internal.INamed;

public interface IPackage extends INamed, INode
{
    boolean isDefault();

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
