package jreflect.structure;

import java.util.List;

import jreflect.Type;
import jreflect.Type.ArrayType;
import jreflect.Type.NumericType;
import jreflect.backend.IVisitor;
import jreflect.structure.internal.IAccessLeveled;
import jreflect.structure.internal.IBodied;
import jreflect.structure.internal.IModifiers;
import jreflect.structure.internal.INamed;

public interface IMethod<R> extends IAccessLeveled, IBodied, IModifiers, INamed, INode
{
    List<IAbstractVariable<?>> getParameters();

    Type<R> getReturnType();

    <T> IArrayVariable<T> newParameter(ArrayType<T> type, String name);

    <T extends Number> INumericVariable<T> newParameter(NumericType<T> type, String name);

    <T> IStandardVariable<T> newParameter(Type<T> type, String name);

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
