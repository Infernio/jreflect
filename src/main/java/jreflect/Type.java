package jreflect;

public class Type<T>
{
    public static final Type<Void> VOID = new Type<>("void");
    public static final Type<Boolean> BOOLEAN = new Type<>("boolean");
    public static final NumericType<Byte> BYTE = new NumericType<>("byte");
    public static final NumericType<Double> DOUBLE = new NumericType<>("double");
    public static final NumericType<Float> FLOAT = new NumericType<>("float");
    public static final NumericType<Integer> INT = new NumericType<>("int");
    public static final NumericType<Long> LONG = new NumericType<>("long");
    public static final NumericType<Short> SHORT = new NumericType<>("short");

    private final String clazzName;

    private Type(final String clazzName)
    {
        this.clazzName = clazzName;
    }

    public String getClassName()
    {
        return this.clazzName;
    }

    public static <T> Type<T> of(final Class<T> clazz)
    {
        return new Type<>(clazz.getName());
    }

    public static <T> ArrayType<T> array(final Type<T> type)
    {
        return new ArrayType<>(type);
    }

    public static class ArrayType<T> extends Type<T>
    {
        private final Type<T> type;

        private ArrayType(final Type<T> type)
        {
            super(type.getClassName());
            this.type = type;
        }

        @Override
        public String toString()
        {
            return this.type.toString() + "[]";
        }
    }

    public static class NumericType<T> extends Type<T>
    {
        private NumericType(final String clazzName)
        {
            super(clazzName);
        }
    }

    @Override
    public String toString()
    {
        return this.getClassName();
    }
}
