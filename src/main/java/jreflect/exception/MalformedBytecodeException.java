package jreflect.exception;

public class MalformedBytecodeException extends Exception
{
    private static final long serialVersionUID = -1243551715070717195L;

    public MalformedBytecodeException()
    {
        super();
    }

    public MalformedBytecodeException(final String msg)
    {
        super(msg);
    }

    public MalformedBytecodeException(final String msg, final Throwable thrown)
    {
        super(msg, thrown);
    }

    public MalformedBytecodeException(final Throwable thrown)
    {
        super(thrown);
    }
}
