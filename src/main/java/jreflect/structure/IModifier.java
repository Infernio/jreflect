package jreflect.structure;

import jreflect.backend.IVisitor;
import jreflect.structure.constant.Modifier;

public interface IModifier extends INode
{
    Modifier asEnum();

    String asJavaKeyword();

    @Override
    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
