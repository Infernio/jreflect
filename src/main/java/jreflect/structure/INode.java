package jreflect.structure;

import jreflect.backend.IVisitor;

public interface INode
{
    INode getParent();

    default void accept(final IVisitor visitor)
    {
        visitor.visit(this);
    }
}
