package jreflect.internal;

import java.util.Locale;
import java.util.Set;

import jreflect.backend.JavaCodeVisitor;
import jreflect.structure.IModifier;
import jreflect.structure.INode;
import jreflect.structure.constant.Modifier;
import lombok.EqualsAndHashCode;
import lombok.val;

@EqualsAndHashCode(callSuper = false)
public final class ModifierImpl extends NodeImpl implements IModifier
{
    private final Modifier mod;

    public ModifierImpl(final INode parent, final Modifier mod)
    {
        super(parent);
        this.mod = mod;
    }

    @Override
    public Modifier asEnum()
    {
        return this.mod;
    }

    @Override
    public String asJavaKeyword()
    {
        return this.mod.name().toLowerCase(Locale.ENGLISH);
    }

    @Override
    public String toString()
    {
        val visitor = new JavaCodeVisitor();
        visitor.visit(this);
        return visitor.finish();
    }

    public static void addAll(final INode parent, final Set<Modifier> from, final Set<IModifier> to)
    {
        from.forEach(mod -> to.add(new ModifierImpl(parent, mod)));
    }

    public static void removeAll(final Set<Modifier> which, final Set<IModifier> from)
    {
        from.removeIf(mod -> which.contains(mod.asEnum()));
    }
}
