package jreflect.util;

import java.util.Map;

import lombok.val;

public class Utils
{
    public static String indent(final String str)
    {
        val ret = new StringBuilder();
        val lines = str.split("\\n");
        for(val line : lines)
        {
            ret.append("    ");
            ret.append(line);
            ret.append('\n');
        }
        return ret.toString();
    }

    public static <K, V> void tryPut(final Map<K, V> map, final K key, final V value, final String error)
    {
        if(!map.containsKey(key))
        {
            map.put(key, value);
        }
        else
        {
            throw new RuntimeException(error);
        }
    }
}
