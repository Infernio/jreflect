package jreflect;

import jreflect.internal.ClassImpl;
import jreflect.structure.IClass;

public class ClassFactory
{
    public static IClass newClass(final String pkg, final String name)
    {
        return new ClassImpl(pkg, name);
    }
}
