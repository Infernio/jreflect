package jreflect.internal;

import java.util.HashSet;
import java.util.Set;

import jreflect.JRSettings;
import jreflect.Type;
import jreflect.backend.JavaCodeVisitor;
import jreflect.structure.IAbstractStatement;
import jreflect.structure.IAbstractValue;
import jreflect.structure.IAccessLevel;
import jreflect.structure.IModifier;
import jreflect.structure.INode;
import jreflect.structure.IStandardField;
import jreflect.structure.constant.Modifier;
import lombok.val;

public final class FieldImpl<T> extends NodeImpl implements IStandardField<T>
{
    private final Type<T> type;
    private String name;
    private final IAccessLevel access;
    private Set<IModifier> modifiers;

    public FieldImpl(final INode parent, final Type<T> type, final String name)
    {
        super(parent);
        this.type = type;
        this.name = name;

        this.access = new AccessLevelImpl(this);
        this.access.setLevel(JRSettings.FieldLevel.DEFAULT_ACCESS_LEVEL.get());

        this.modifiers = new HashSet<>();
        ModifierImpl.addAll(this, JRSettings.FieldLevel.DEFAULT_MODIFIERS.get(), this.modifiers);
    }

    @Override
    public IAccessLevel getAccess()
    {
        return this.access;
    }

    @Override
    public void addModifiers(final Modifier... mods)
    {
        ModifierImpl.addAll(this, Set.of(mods), this.modifiers);
    }

    @Override
    public void removeModifiers(final Modifier... mods)
    {
        ModifierImpl.removeAll(Set.of(mods), this.modifiers);
    }

    @Override
    public Set<IModifier> getModifiers()
    {
        return this.modifiers;
    }

    @Override
    public void setModifiers(final Set<IModifier> to)
    {
        this.modifiers = to;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(final String to)
    {
        this.name = to;
    }

    @Override
    public Type<T> getType()
    {
        return this.type;
    }

    @Override
    public IAbstractStatement<Void> assign(final T value)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IAbstractStatement<?> invokeMethod(final String string, final IAbstractValue<?>... values)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String toString()
    {
        val visitor = new JavaCodeVisitor();
        visitor.visit(this);
        return visitor.finish();
    }
}
